#! /bin/bash
# Febrer 2020
# @ edt ASIX-M01 Curs 2019-2020
# Validar si es un file dir o link
# --------------------------------------------------

# Validar #args
ERR_NARGS=1
if [ $# -ne 1 ];then
	echo "Error: #args incorrecte"
	echo "Usage: $0 directori"
	exit $ERR_NARGS
fi


element=$1
if [ ! -e $element ];then
	echo "$element no existeix"

elif [ -h $element ];then
	echo "$element es un symbolic link"
	
elif [ -f $element ];then
	echo "$element es un regular file"
	
elif [ -d $element ];then
	echo "$element es un directori"
else
	echo "Es un altre cosa"
fi

exit 0
