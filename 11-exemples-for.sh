# Gener 2020
# Alejandro Gambín
# validacio synopsis
# Gener 2020
# Descripció: Exemples bucle for
# -------------------------------
OK=0
#Mostrar i númerar llista d'arguments
count=1
for arg in $*
do 
	echo "$count: $arg"
	count=$((count+1))
done
exit $OK

# Iterar per la llista d'arguments
for nom in $* 
do	
	echo $nom
done

exit $OK


# Iterar per un contingut d'una variable
llistat=$(ls)
for nom in $llistat 
do
	echo $llistat
done 
exit $OK




# Iterar per el contingut d'una cadena
# Només itera un cop
for nom in "pere pau marta anna"
do
	echo $nom
done 
exit $OK

# Iterar per un conjunt d'elements
for nom in "pere" "pau" "marta" "anna"
do
	echo $nom
done 
exit $OK
