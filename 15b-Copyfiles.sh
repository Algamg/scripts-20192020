#! /bin/bash 
# Alejandro Gambín
# Febrer 2020
# Hacer un cp de varios regulars files a un directorio 
# ------------------------------------------
OK=0
# Validació #args 

file=$1
dir=$2
#if ! [ -e file -a -e dir ];then
#        echo "Error: Alguno de los valores no existe "
#        echo "Usage: $0 file dir"
#        exit $ERR_NOEXIST
#fi
if ! [ -f $file ];then
    echo "Error: $file no es un regular file" 
    echo "Usage: $0 file dir"
    exit $ERR_NOFILE
fi

if ! [ -d $dir ];then
    echo "Error: $dir no es un directori"
    echo "Usage: $0 file dir"
    exit $ERR_NODIR
fi

cp $file $dir


# Con shift 
# Mientras que $1 exista vamos copiando a $#

while $1 
