ERR_DIR=1
while [ -n "$2" ]
do 
    if ! [ -f $1 ];then
      echo "Error: $1 no es un regular file" >> /dev/stderr
      echo "Usage: $0 files dir" >> /dev/stderr
    fi	
    dir=$#
    $$dir 
    if ! [ -d $$dir  ];then
        echo "Error: $# no es un directorio" >> /dev/stderr
        echo "Usage: $0 files dir" >> /dev/stderr
        exit $ERR_DIR        
    fi

    cp $1 $#
	shift
done
exit 0