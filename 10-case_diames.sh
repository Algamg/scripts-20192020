# Gener 2020
# Alejandro Gambín
# Descripcio: Stdout quins dia te el mes  
# ----------------------------------------
ERR_NARGS=1
ERR_ARGVL=2
OK=0
# Validar argument


if [ $# -ne 1 ];then
        echo "Error: #args incorrecte"
	echo "Usage: $0 arg1 arg2"
	exit $ERR_NARGS
fi

if [ $1 = "-h" ];then

	echo "@edt ASIX-M01"
        echo "Alejandro Gambín Gómez"
	exit $OK
fi
mes=$1

if ! [ $mes -ge 1 -a $mes -le 12 ];then
	echo "Error: mes no vàlid"
	echo "Usage: $0 [1-12]"
	exit $ERR_ARGVL
fi

case $mes in
	"1" | "3" | "5" | "7" | "8" | "10" | "12")
		dies="31";;
	"2")
		dies="28";;
	"4" | "6" | "9" | "11")
		dies="30";;
esac
echo $dies
exit $OK
