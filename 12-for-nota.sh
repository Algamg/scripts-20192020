#! /bin/bash
# @ edt ASIX-M01 Curs 2019-2020
# Descripció: processar nota a nota i 
# 	indicar si suspès, aprovat, notable
#	o excel·lent
# prog nota[...]
# ---------------------------------------------------
# Validar #args 
	#Si $# != 1 --> Error [msg error , msg usage , exit 1]
ERR_NARGS=1
OK=0
if [ $# -eq 0 ];then
        echo "Error: #args incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_NARGS
fi

# Validar nota valida

llistaNotes=$*
for nota in $llistaNotes
do
	# Si $nota no esta entre 0 y 10 --> Error [msg error, msg usage, exit 2]
	if ! [ $nota -ge 0 -a $nota -le 10 ];then
		echo "Error: La nota $nota no és vàlida" >> /dev/stderr
		echo "Valors vàlids: 0 - 10" >> /dev/stderr
	elif [ $nota -lt 5 ];then
	        echo "Suspès"
	elif [ $nota -lt 7 ];then
	        echo "Aprovat"
	elif [ $nota -le 8 ];then
	        echo "Notable"
	else
	        echo "Excel·lent"
	fi	
done

exit $OK

