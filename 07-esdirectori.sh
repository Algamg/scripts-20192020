#! /bin/bash
# Febrer 2020
# @ edt ASIX-M01 Curs 2019-2020
# Validar nota: suspès, aprovat, notable, excel·lent
# --------------------------------------------------

# Validar #args

if [ $# -ne 1 ];then
	echo "Error: #args incorrecte"
	echo "Usage: $0 directori"
	exit 1
fi


if ! [ -d $1 ];then
	echo "Error: $1 no es un directori"
	echo "Usage: $0 dir"
	exit 2
fi

#xixa
directori=$1
ls $directori
exit 0
