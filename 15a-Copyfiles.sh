#! /bin/bash 
# Alejandro Gambín
# Febrer 2020
# Fer un cp d'un regular file a un dir 
# ------------------------------------
OK=0
ERR_NARGS=1
#ERR_NOEXIST=2
ERR_NOFILE=2
ERR_NODIR=3
# Validació #args 

if [ $# -ne 2 ];then
        echo "Error: #args incorrecte"
        echo "Usage: $0 file dir"
        exit $ERR_NARGS
fi

file=$1
dir=$2
#if ! [ -e file -a -e dir ];then
#        echo "Error: Alguno de los valores no existe "
#        echo "Usage: $0 file dir"
#        exit $ERR_NOEXIST
#fi
if ! [ -f $file ];then
    echo "Error: $file no es un regular file"
    echo "Usage: $0 file dir"
    exit $ERR_NOFILE
fi

if ! [ -d $dir ];then
    echo "Error: $dir no es un directori"
    echo "Usage: $0 file dir"
    exit $ERR_NODIR
fi

cp $file $dir

