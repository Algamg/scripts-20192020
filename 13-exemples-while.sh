#! /bin/bash
# @ edt ASIX-M01 Curs 2019-2020
# Febrer 2020
# Descripció: Exemples Bucle While
#---------------------------------------------------

# Comptador recreixent del argument rebut

# Utilitzan shift mostri enumerats els arguments
# 9) Mostri tot el que rep de stdin a Mayus
num=1
while read -r line
do
	echo "$num: $line" | tr '[a-z]' '[A-Z]'
	num=$((num+1))

done
exit  0

read -r line
while [ $line != "FI" ]
do
	echo $line	
	read -r line
done

exit 0


while read -r line
do 
	echo $line
	
done	

exit 0




comptador=1
while [ -n "$1" ]
do 
	echo "$comptador: $1"
	shift
	comptador=$((comptador+1))
done

exit 0


while [ -n "$1" ]
do 
	echo "$1 $#: $*"
	shift
done
exit 0


# Mostrar els arguments
while [ $# -gt 0 ]
do
	echo "$#: $*"
	shift 
done
exit 0





valor=$1
MIN=0
while [ $valor -ge $min ]
do
	echo "$valor"
	valor=$((valor-1))
done
exit 0


num=1
MAX=10
while [ $num -le $MAX ] 
do
	echo "$num"
	num=$((num+1))
done
exit 0
