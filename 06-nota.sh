#! /bin/bash
# @ edt ASIX-M01 Curs 2019-2020
# Validar nota: suspès, aprovat, notable, excel·lent
# ---------------------------------------------------


# Validar #args 
	#Si $# != 1 --> Error [msg error , msg usage , exit 1]
if [ $# -ne 1 ];then
        echo "Error: #args incorrecte"
	echo "Usage: $0 nota"
	exit 1
fi

# Validar nota valida
	# Si el $1 no esta entre 0 y 10 --> Error [msg error, msg usage, exit 2]

if ! [ $1 -ge 0 -a $1 -le 10 ];then
	echo "Error: nota $1 no valida [0-10]"
	echo "Usage: $0 nota"
	exit 2
fi

nota=$1

if [ $nota -lt 5 ];then
	echo "Suspès"
elif [ $nota -lt 7 ];then
	echo "Aprovat"
elif [ $nota -le 8 ];then
	echo "Notable"
else 
	echo "Excel·lent"	
fi
exit 0

