#! /bin/bash 
# Alejandro Gambín
# Febrer 2020
# Llistar un dir
# ------------------------------------
OK=0
ERR_NARGS=1
ERR_NODIR=2
# Validació #args 

if [ $# -ne 1 ];then
        echo "Error: #args incorrecte"
        echo "Usage: $0 directori"
        exit $ERR_NARGS
fi

dir=$1
if ! [ -d $dir ];then
	echo "Error: $dir no es un directori"
	echo "Usage: $0 directori"
	exit $ERR_NODIR 
fi


# Per cada element hem de dir si es un Link, Regular file o una altre cosa
dir=$1
llistat=$(ls $dir)
cd $dir 
num=1
for x in $llistat
do
    if [ -h $x ];then
		echo "$num: $x es un symbolic link"
	elif [ -d $x ];then
		echo "$num: $x es un directori"
	elif [ -f $x ];then 
		echo "$num: $x es un regular file"
	else
		echo "$num: $x es una altre cosa"
	fi
	num=$((num+1))
done
exit 0







exit $OK
