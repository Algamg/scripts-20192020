#! /bin/bash
# Alejandro Gambín
# Gener 2020
# exemple if 
#  $ prog edat 
# ----------------------------------------
# Validar argument
if [ ! $# -eq 1 ];then
	echo "No has ficat arguments o has introduit més d'un argument"
	echo "Usage: $0 edat"
	exit 1
fi

# xixa 

edat=$1

if [ $edat -ge 18 ];then
	echo "edat $edat es major d'edat"

else 
	echo "edat $edat es menor d'edat"

fi
